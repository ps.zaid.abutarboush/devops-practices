FROM openjdk:8-jdk-alpine

ENV SPRING_PROFILE="h2"

WORKDIR /data

COPY target/assignment_*.jar /data

EXPOSE 8090

ENTRYPOINT java -jar -Dspring.profile.active=${SPRING_PROFILE} assignment_*.jar

# Build command
# docker build . -t devops-practice:`basename target/assignment_*.jar | cut -b 12-27`
# Note: make sure to bind to the host network upon running
